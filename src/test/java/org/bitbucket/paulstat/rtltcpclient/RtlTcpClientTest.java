/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.List;

import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class RtlTcpClientTest {

	private RtlTcpClient client;
	private InputStream is;

	@Before
	public void init() {
		client = new RtlTcpClient(TestUtils.VALID_CONFIG);
		is = TestUtils.createValidRtlTcpInputStreamHeader();
	}

	@Test
	public void testConnect(@Mocked Socket socket, @Mocked RtlTcpCommand command) throws Exception {

		new MockUp<RtlTcpDongleInfoFactory>() {
			@Mock
			RtlTcpDongleInfo createRtlTcpDongleInfo(Socket socket) throws IOException {
				return TestUtils.VALID_DONGLE;
			}
		};
		
		new MockUp<RtlTcpUtils>() {
			@Mock
			List<RtlTcpCommand> toCommandList(RtlTcpConnectionConfig config, RtlTcpDongleInfo dongleInfo) throws RtlTcpCommandException {
				return TestUtils.ALL_COMMANDS;
			}
		};
		
		new Expectations() {
			{
				new Socket(TestUtils.VALID_CONFIG.getHostName(), TestUtils.VALID_CONFIG.getPort());
				result = socket;
								
				socket.getInputStream();
				result = is;
			}
		};

		client.connect();

		new Verifications() {
			{
				command.execute(socket);
				times = 12;
			}
		};

	}
	
	@Test
	public void testConnectThrowsRtlTcpConnectionExceptionOnInvalidDongleInfo(@Mocked Socket socket) {
		
		new MockUp<RtlTcpDongleInfoFactory>() {
			@Mock
			RtlTcpDongleInfo createRtlTcpDongleInfo(Socket socket) throws IOException {
				return TestUtils.INVALID_DONGLE;
			}
		};
		
		try {
			client.connect();
		} catch (RtlTcpException e) {
			final String expectedMessage = "Invalid magic number, expected RTL0 got RTL1";
			
			String message = e.getMessage();
			
			assertThat(message, equalTo(expectedMessage));
		}
		
	}
	
	@Test
	public void testSetAGCModeThrowsRtlTcpConnectionExcpetionWhenNotConnected() {
		
		try {
			client.setAGCMode(true);
		} catch (RtlTcpException e) {
			final String expectedMessage = "Socket connection closed";
			
			String message = e.getMessage();
			
			assertThat(message, equalTo(expectedMessage));
		}
		
	}

	@Test
	public void testConnectThrowsRtlTcpConnectionExceptionOnIOException(@Mocked Socket socket) throws Exception {

		try {
			
			new Expectations() {
				{
					new Socket("blah", 80);
					result = new IOException("Socket constructor exception");
				}
			};
			
			client.connect();
		} catch (RtlTcpConnectionException e) {
			String message = e.getMessage();

			assertThat(message, equalTo("Failed to connect to blah:80: "));

			if (e.getCause() instanceof IOException) {
				Throwable t = e.getCause();
				assertThat(t.getMessage(), equalTo("Socket constructor exception"));
			} else {
				fail("Unexpected cause");
			}
		}

	}

	@Test
	public void testDisconnect(@Mocked Socket socket) throws Exception {

		new Expectations() {
			{
				socket.getInputStream();
				result = is;
			}
		};
		
		client.connect();
		client.disconnect();

		assertNull(client.getConnection());

		new Verifications() {
			{
				socket.close();
			}
		};
	}

	@Test
	public void testDisconnectThrowsRtlTcpConnectionExceptionOnIOException(@Mocked Socket socket) throws Exception {

		try {
			new Expectations() {
				{
					socket.getInputStream();
					result = is;
					
					socket.close();
					result = new IOException("Close");
				}
			};

			client.connect();
			client.disconnect();
		} catch (RtlTcpConnectionException e) {
			String message = e.getMessage();

			assertThat(message, equalTo("Failed to disconnect: "));

			if (e.getCause() instanceof IOException) {
				Throwable t = e.getCause();
				assertThat(t.getMessage(), equalTo("Close"));
			} else {
				fail("Unexpected cause");
			}
		}

	}
	
	@Test
	public void testDisconnectWhenSocketIsClosed(@Mocked Socket socket) throws Exception {
		
		new Expectations() {
			{
				socket.isClosed();
				result = true;
				
				socket.getInputStream();
				result = is;
			}
		};
		
		client.connect();
		client.disconnect();
		
		new Verifications() {
			{
				socket.close();
				times = 0;
			}
		};
		
	}
	
	@Test
	public void testDisconnectWhenSocketIsNull(@Mocked Socket socket) throws Exception {
		client.disconnect();
		
		new Verifications() {
			{
				socket.close();
				times = 0;
			}
		};
	}

}
