/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.bitbucket.paulstat.rtltcpclient.RtlTcpConnectionConfig;
import org.bitbucket.paulstat.rtltcpclient.RtlTcpUtils;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommand;
import org.junit.Test;
import org.junit.runner.RunWith;

import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class RtlTcpUtilsTest {
	
	private static final RtlTcpDongleInfo DONGLE_INFO = new RtlTcpDongleInfo(RtlTcpDongleInfo.VALID_MAGIC_NUMBER.getBytes(),
			5, 80);
	
	@Test
	public void testToCommandListAllCommandsIncluded() throws Exception {
		RtlTcpConnectionConfig config = new RtlTcpConnectionConfig.Builder()
				.withCenterFrequency(80)
				.withFrequencyCorrection(80)
				.withGainByIndex(80)
				.withHostName("blah")
				.withPort(80)
				.withRtlXtalFrequency(80)
				.withSampleRate(80)
				.withTunerGain(80)
				.withTunerXtalFrequency(80)
				.enableAgcMode()
				.enableDirectSampling()
				.enableOffsetTuning()
				.enableTestMode()
				.enableTunerGainMode().build();
		
		List<RtlTcpCommand> commandList = RtlTcpUtils.toCommandList(config, DONGLE_INFO);
		
		final int expectedListSize = 12;
		
		assertThat(commandList.size(), equalTo(expectedListSize));
	}
	
	@Test
	public void testToCommandListEmpty() throws Exception {
		RtlTcpConnectionConfig config = new RtlTcpConnectionConfig.Builder()
				.withHostName("blah")
				.withPort(80)
				.build();
		
		List<RtlTcpCommand> commandList = RtlTcpUtils.toCommandList(config, DONGLE_INFO);
		
		final int expectedListSize = 1;
		
		assertThat(commandList.size(), equalTo(expectedListSize));
	}

}
