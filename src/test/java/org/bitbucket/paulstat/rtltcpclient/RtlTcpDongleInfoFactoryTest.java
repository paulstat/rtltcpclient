/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.bitbucket.paulstat.rtltcpclient.RtlTcpDongleInfo;
import org.bitbucket.paulstat.rtltcpclient.RtlTcpDongleInfoFactory;
import org.junit.Test;
import org.junit.runner.RunWith;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class RtlTcpDongleInfoFactoryTest {
	
	@Test
	public void testCreateRtlTcpDongleInfoValid(@Mocked Socket socket, @Mocked RtlTcpDongleInfo dongleInfo) throws Exception {
		new Expectations() {
			{
				socket.getInputStream();
				result = TestUtils.createValidRtlTcpInputStreamHeader();
			}
		};
		
		RtlTcpDongleInfoFactory.createRtlTcpDongleInfo(socket);
		
		final byte[] expectedMagicNumber = new byte[]{82, 84, 76, 48};
		final int expectedTunerType = 5;
		final int expectedGainCount = 29;
		
		new Verifications() {
			{
				new RtlTcpDongleInfo(withEqual(expectedMagicNumber), expectedTunerType, expectedGainCount);
			}
		};
		
	}
	
	@Test
	public void testCreateRtlTcpDongleInfoShortHeader(@Mocked Socket socket) {
		assertBadRtlTcpHeader(socket, TestUtils.createShortRtlTcpInputStreamHeader());
	}
	
	@Test
	public void testCreateRtlTcpDongleInfoInterruptedConnection(@Mocked Socket socket) {
		assertBadRtlTcpHeader(socket, TestUtils.createInterruptedRtlTcpInputStreamHeader());
	}
	
	@Test
	public void testCreateRtlTcpDongleInfoEmptyHeader(@Mocked Socket socket) {
		assertBadRtlTcpHeader(socket, TestUtils.createEmptyRtlTcpInputStreamHeader());
	}
	
	/**
	 * Assert the expected exception message from a bad RTL TCP header
	 * 
	 * @param socket
	 * @param is
	 */
	private void assertBadRtlTcpHeader(Socket socket, InputStream is) {
		try {
			new Expectations() {
				{
					socket.getInputStream();
					result = is;
				}
			};
			
			RtlTcpDongleInfoFactory.createRtlTcpDongleInfo(socket);
		} catch (IOException e) {
			final String expectedMessage = "Closed connection detected";
			
			String message = e.getMessage();

            assertEquals(expectedMessage, message);
		}
	}

}
