/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetAGCModeCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetCenterFrequencyCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetDirectSamplingCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetFrequencyCorrectionCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetGainModeCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetOffsetTuningCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetRtlXtalFrequencyCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetSampleRateCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetTestModeCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetTunerGainByIndexCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetTunerGainCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpSetTunerXtalFrequencyCommand;

public final class TestUtils {

	private static final String VALID_RTL_TCP_HEADER = new String(
			new byte[] { 82, 84, 76, 48, 0, 0, 0, 5, 0, 0, 0, 29 });
	private static final String INVALID_RTL_TCP_HEADER = new String(
			new byte[] { 12, 78, 66, 77, 0, 0, 0, 5, 0, 0, 0, 29 });
	private static final String EMPTY_RTL_TCP_HEADER = "";
	private static final String INTERRUPTED_RTL_TCP_HEADER = new String(new byte[] { 82, 84, 76, 48, -1 });
	private static final String SHORT_RTL_TCP_HEADER = new String(new byte[] { 82, 84, 76, 48 });

	public static final RtlTcpConnectionConfig VALID_CONFIG = new RtlTcpConnectionConfig.Builder()
			.withCenterFrequency(80)
			.withFrequencyCorrection(80)
			.withGainByIndex(29)
			.withHostName("blah").withPort(80)
			.withRtlXtalFrequency(80)
			.withSampleRate(80)
			.withTunerGain(80)
			.withTunerXtalFrequency(80)
			.enableAgcMode()
			.enableDirectSampling()
			.enableOffsetTuning()
			.enableTestMode()
			.enableTunerGainMode().build();

	public static final RtlTcpDongleInfo VALID_DONGLE = new RtlTcpDongleInfo(
			RtlTcpDongleInfo.VALID_MAGIC_NUMBER.getBytes(), 5, 29);
	
	public static final RtlTcpDongleInfo INVALID_DONGLE = new RtlTcpDongleInfo(
			"RTL1".getBytes(), 5, 29);
	
	public static List<RtlTcpCommand> ALL_COMMANDS;
	
	static {
		try {
		ALL_COMMANDS = 	Arrays.asList(
				new RtlTcpSetCenterFrequencyCommand(80),
				new RtlTcpSetFrequencyCorrectionCommand(80),
				new RtlTcpSetTunerGainByIndexCommand(VALID_DONGLE, 29),
				new RtlTcpSetRtlXtalFrequencyCommand(80),
				new RtlTcpSetSampleRateCommand(80),
				new RtlTcpSetTunerGainCommand(80),
				new RtlTcpSetTunerXtalFrequencyCommand(80),
				new RtlTcpSetAGCModeCommand(true),
				new RtlTcpSetDirectSamplingCommand(true),
				new RtlTcpSetOffsetTuningCommand(true),
				new RtlTcpSetTestModeCommand(true),
				new RtlTcpSetGainModeCommand(true));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<RtlTcpCommand> EMPTY_COMMAND_LIST = new ArrayList<>();


	/**
	 * Can't be instantiated
	 */
	private TestUtils() {

	}

	public static final InputStream createValidRtlTcpInputStreamHeader() {
		return new ByteArrayInputStream(VALID_RTL_TCP_HEADER.getBytes());
	}

	public static final InputStream createInvalidRtlTcpInputStreamHeader() {
		return new ByteArrayInputStream(INVALID_RTL_TCP_HEADER.getBytes());
	}

	public static final InputStream createEmptyRtlTcpInputStreamHeader() {
		return new ByteArrayInputStream(EMPTY_RTL_TCP_HEADER.getBytes());
	}

	public static final InputStream createInterruptedRtlTcpInputStreamHeader() {
		return new ByteArrayInputStream(INTERRUPTED_RTL_TCP_HEADER.getBytes());
	}

	public static final InputStream createShortRtlTcpInputStreamHeader() {
		return new ByteArrayInputStream(SHORT_RTL_TCP_HEADER.getBytes());
	}

}
