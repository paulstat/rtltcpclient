/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

/**
 * Connection config POJO for RTL_TCP
 * 
 * @author Paul
 */
public class RtlTcpConnectionConfig {
	
	private boolean agcMode;	
	private int centerFrequency;
	private boolean directSampling;
	private int frequencyCorrection;
	private int gainByIndex;
	private boolean offsetTuning;
	private int rtlXtalFrequency;
	private int sampleRate;
	private String hostName;
	private int port;
	private boolean testMode;
	private int tunerGain;
	private boolean tunerGainMode;
	private int tunerXtalFrequency;
	
	/**
	 * Static Builder class for creating RtlTcpConnectionConfig instances
	 *
	 * @author Paul
	 */
	public static class Builder {
		
		private boolean agcMode;	
		private int centerFrequency;
		private boolean directSampling;
		private int frequencyCorrection;
		private int gainByIndex;
		private boolean offsetTuning;
		private int rtlXtalFrequency;
		private int sampleRate;
		private String hostName;
		private int port;
		private boolean testMode;
		private int tunerGain;
		private boolean tunerGainMode;
		private int tunerXtalFrequency;
		
		public Builder enableAgcMode() {
			this.agcMode = true;
			return this;
		}
		
		public Builder withCenterFrequency(int centerFrequency) {
			this.centerFrequency = centerFrequency;
			return this;
		}
		
		public Builder enableDirectSampling() {
			this.directSampling = true;
			return this;
		}
		
		public Builder withFrequencyCorrection(int frequencyCorrection) {
			this.frequencyCorrection = frequencyCorrection;
			return this;
		}
		
		public Builder withGainByIndex(int gainByIndex) {
			this.gainByIndex = gainByIndex;
			return this;
		}
		
		public Builder enableOffsetTuning() {
			this.offsetTuning = true;
			return this;
		}
		
		public Builder withRtlXtalFrequency(int rtlXtalFrequency) {
			this.rtlXtalFrequency = rtlXtalFrequency;
			return this;
		}
		
		public Builder withSampleRate(int sampleRate) {
			this.sampleRate = sampleRate;
			return this;
		}
		
		public Builder withHostName(String hostName) {
			this.hostName = hostName;
			return this;
		}
		
		public Builder withPort(int port) {
			this.port = port;
			return this;
		}
		
		public Builder enableTestMode() {
			this.testMode = true;
			return this;
		}
		
		public Builder withTunerGain(int tunerGain) {
			this.tunerGain = tunerGain;
			return this;
		}
		
		public Builder enableTunerGainMode() {
			this.tunerGainMode = true;
			return this;
		}
		
		public Builder withTunerXtalFrequency(int tunerXtalFrequency) {
			this.tunerXtalFrequency = tunerXtalFrequency;
			return this;
		}
		
		/**
		 * Build a new instance of RtlTcpConnectionConfig
		 * 
		 * @return RtlTcpConnectionConfig instance
		 */
		public RtlTcpConnectionConfig build() {
			return new RtlTcpConnectionConfig(agcMode, centerFrequency, directSampling, frequencyCorrection,
					gainByIndex, offsetTuning, rtlXtalFrequency, sampleRate, hostName, port, testMode,
					tunerGain, tunerGainMode, tunerXtalFrequency);
		}
		
	}
	
	/**
	 * Creates a new instance
	 * 
	 * @param agcMode
	 * @param centerFrequency
	 * @param directSampling
	 * @param frequencyCorrection
	 * @param gainByIndex
	 * @param offsetTuning
	 * @param rtlXtalFrequency
	 * @param sampleRate
	 * @param hostName
	 * @param port
	 * @param testMode
	 * @param tunerGain
	 * @param tunerGainMode
	 * @param tunerXtalFrequency
	 */
	public RtlTcpConnectionConfig(boolean agcMode, int centerFrequency, boolean directSampling,
			int frequencyCorrection, int gainByIndex, boolean offsetTuning, int rtlXtalFrequency,
			int sampleRate, String hostName, int port, boolean testMode, int tunerGain, boolean tunerGainMode,
			int tunerXtalFrequency) {
		this.agcMode = agcMode;
		this.centerFrequency = centerFrequency;
		this.directSampling = directSampling;
		this.frequencyCorrection = frequencyCorrection;
		this.gainByIndex = gainByIndex;
		this.offsetTuning = offsetTuning;
		this.rtlXtalFrequency = rtlXtalFrequency;
		this.sampleRate = sampleRate;
		this.hostName = hostName;
		this.port = port;
		this.testMode = testMode;
		this.tunerGain = tunerGain;
		this.tunerGainMode = tunerGainMode;
		this.tunerXtalFrequency = tunerXtalFrequency;
	}

	/**
	 * Checks if Automatic Gain Control is enabled
	 * 
	 * @return
	 */
	public boolean isAgcMode() {
		return agcMode;
	}
	
	/**
	 * Center Frequency to receive on
	 * 
	 * @return
	 */
	public int getCenterFrequency() {
		return centerFrequency;
	}
	
	/**
	 * Checks if Direct Sampling is enabled
	 * 
	 * @return
	 */
	public boolean isDirectSampling() {
		return directSampling;
	}
	
	/**
	 * Frequency Correction in Parts Per Million (ppm)
	 * 
	 * @return
	 */
	public int getFrequencyCorrection() {
		return frequencyCorrection;
	}
	
	/**
	 * Gain By Index
	 * 
	 * @return
	 */
	public int getGainByIndex() {
		return gainByIndex;
	}
	
	/**
	 * Check if Offset tuning is enabled
	 * 
	 * @return
	 */
	public boolean isOffsetTuning() {
		return offsetTuning;
	}
	
	/**
	 * Realtech crystal Frequency
	 * 
	 * @return
	 */
	public int getRtlXtalFrequency() {
		return rtlXtalFrequency;
	}
	
	/**
	 * Audio samples per second in Hz
	 * 
	 * @return
	 */
	public int getSampleRate() {
		return sampleRate;
	}
	
	/**
	 * RTL_TCP Server Hostname
	 * 
	 * @return
	 */
	public String getHostName() {
		return hostName;
	}
	
	/**
	 * RTL_TCP Server port
	 * 
	 * @return
	 */
	public int getPort() {
		return port;
	}
	
	/**
	 * Check test mode enabled
	 * 
	 * @return
	 */
	public boolean isTestMode() {
		return testMode;
	}
	
	/**
	 * Power/Amplitude gain in dB
	 * 
	 * @return
	 */
	public int getTunerGain() {
		return tunerGain;
	}
	
	/**
	 * Check tuner gain mode
	 * 
	 * @return
	 */
	public boolean isTunerGainMode() {
		return tunerGainMode;
	}
	
	/**
	 * Tuner crystal frequency
	 * 
	 * @return
	 */
	public int getTunerXtalFrequency() {
		return tunerXtalFrequency;
	}
	
}
