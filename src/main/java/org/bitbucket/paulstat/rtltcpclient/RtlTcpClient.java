/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandException;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandFactory;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandType;

/**
 * Client for receiving input from and controlling an RTL_TCP server
 * 
 * @author Paul
 */
public class RtlTcpClient {

	private RtlTcpConnectionConfig rtlTcpConfig;
	private Socket connection;
	private RtlTcpDongleInfo dongleInfo;
	private RtlTcpCommandFactory commandFactory;

	/**
	 * Creates a new instance
	 * 
	 * @param rtlTcpConfig
	 *            settings
	 */
	public RtlTcpClient(RtlTcpConnectionConfig rtlTcpConfig) {
		this.rtlTcpConfig = rtlTcpConfig;
	}

	/**
	 * Opens a Socket connection to the RTL_TCP server and executes commands
	 * which match that of the RtlTcpConnectionConfig
	 * 
	 * @return InputStream of the connection
	 * @throws RtlTcpException
	 */
	public InputStream connect() throws RtlTcpException {
		try {
			connection = new Socket(rtlTcpConfig.getHostName(), rtlTcpConfig.getPort());

			dongleInfo = RtlTcpDongleInfoFactory.createRtlTcpDongleInfo(connection);

			if (!dongleInfo.isValid()) {
				throw new RtlTcpConnectionException("Invalid magic number, expected "
						+ RtlTcpDongleInfo.VALID_MAGIC_NUMBER + " got " + dongleInfo.getMagicNumber());
			}
			
			commandFactory = new RtlTcpCommandFactory(dongleInfo);

			for (RtlTcpCommand command : RtlTcpUtils.toCommandList(rtlTcpConfig, dongleInfo)) {
				command.execute(connection);
			}

			return connection.getInputStream();
		} catch (IOException e) {
			throw new RtlTcpConnectionException(
					"Failed to connect to " + rtlTcpConfig.getHostName() + ":" + rtlTcpConfig.getPort() + ": ", e);
		}
	}

	/**
	 * Closes the Socket connection to the RTL_TCP server if the socket is not
	 * already closed
	 * 
	 * @throws RtlTcpConnectionException
	 */
	public void disconnect() throws RtlTcpConnectionException {
		try {
			if (connectionOpen()) {
				connection.close();
				connection = null;
			}
		} catch (IOException e) {
			throw new RtlTcpConnectionException("Failed to disconnect: ", e);
		}
	}
	
	/**
	 * Enables/Disables AGC mode
	 * 
	 * @param modeState
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setAGCMode(boolean modeState) throws RtlTcpException {
	    executeCommand(RtlTcpCommandType.SET_AGC_MODE, modeState);
	}
	
	/**
	 * Set Center Frequency of RTL-SDR
	 * 
	 * @param centerFrequency
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setCenterFrequency(int centerFrequency) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_FREQUENCY, centerFrequency);
	}
	
	/**
	 * Enables/Disables direct sampling mode
	 * 
	 * @param directSampling
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setDirectSampling(boolean directSampling) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_DIRECT_SAMPLING, directSampling);
	}
	
	/**
	 * Set Frequency Correction
	 * 
	 * @param frequencyCorrection
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setFrequencyCorrection(int frequencyCorrection) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_FREQ_CORRECTION, frequencyCorrection);
	}
	
	/**
	 * Emables/Disables gain mode
	 * 
	 * @param gainMode
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setGainMode(boolean gainMode) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_GAIN_MODE, gainMode);
	}
	
	/**
	 * Enables/Disables Offset Tuning
	 * 
	 * @param offsetTuning
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setOffsetTuning(boolean offsetTuning) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_OFFSET_TUNING, offsetTuning);
	}
	
	/**
	 * Set RTL Crystal Frequency
	 * 
	 * @param rtlXtalFrequency
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setRtlXtalFrequency(int rtlXtalFrequency) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_RTL_XTAL_FREQ, rtlXtalFrequency);
	}
	
	/**
	 * Set Sample Rate
	 * 
	 * @param sampleRate
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setSampleRate(int sampleRate) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_SAMPLE_RATE, sampleRate);
	}
	
	/**
	 * Enables/Disables test mode
	 * 
	 * @param testMode
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setTestMode(boolean testMode) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_TEST_MODE, testMode);
	}
	
	/**
	 * Set Tuner Gain By Index
	 * 
	 * @param tunerGainByIndex
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setTunerGainByIndex(int tunerGainByIndex) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_TUNER_GAIN_BY_INDEX, tunerGainByIndex);
	}
	
	/**
	 * Set Tuner Gain
	 * 
	 * @param tunerGain
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setTunerGain(int tunerGain) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_TUNER_GAIN, tunerGain);
	}
	
	/**
	 * Set Tuner Intermediate Frequency Gain
	 * 
	 * @param tunerIFGain
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setTunerIFGain(int tunerIFGain) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_TUNER_IF_GAIN, tunerIFGain);
	}
	
	/**
	 * Set Tuner Xtal Frequency
	 * 
	 * @param tunerXtalFrequency
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	public void setTunerXtalFrequency(int tunerXtalFrequency) throws RtlTcpException {
		executeCommand(RtlTcpCommandType.SET_TUNER_XTAL_FREQ, tunerXtalFrequency);
	}
	
	/**
	 * Execute the given command type with the specified value
	 * 
	 * @param commandType
	 * @param commandValue
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	private void executeCommand(RtlTcpCommandType commandType, int commandValue) throws RtlTcpException {
		if (!connectionOpen()) {
		    throw new RtlTcpConnectionException("Socket connection closed");	
		}
		
		executeCommand(commandFactory.createCommand(commandType, commandValue));
	}
	
	/**
	 * Execute the given command type with the specified value
	 * 
	 * @param commandType
	 * @param commandValue
	 * @throws RtlTcpException if not connected or if something goes wrong with
	 * the command execution
	 */
	private void executeCommand(RtlTcpCommandType commandType, boolean commandValue) throws RtlTcpException {
		if (!connectionOpen()) {
		    throw new RtlTcpConnectionException("Socket connection closed");	
		}
		
		executeCommand(commandFactory.createCommand(commandType, commandValue));
	}
	
	/**
	 * Executes the given command on the socket
	 * 
	 * @param command
	 * @throws RtlTcpException
	 */
	private void executeCommand(RtlTcpCommand command) throws RtlTcpException {
		try {
			command.execute(connection);
		} catch (IOException e) {
			throw new RtlTcpCommandException("Failed to execute command: " + command, e);
		}
	}
	
	/**
	 * Check status of socket
	 * 
	 * @return
	 */
	private boolean connectionOpen() {
		return connection != null && !connection.isClosed();
	}

	/**
	 * Socket connection to RTL_TCP inatance
	 * 
	 * @return
	 */
	public Socket getConnection() {
		return connection;
	}

}
