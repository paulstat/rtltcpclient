/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

/**
 * RTL SDR Dongle Info
 *
 * @author Paul
 */
public class RtlTcpDongleInfo {
	
	public static final String VALID_MAGIC_NUMBER = "RTL0";
	
	enum DongleType {
		E4000,
		FC0012,
		FC0013,
		FC2580,
		R820T,
		R828D,
		UNKNOWN;
	}
	
	private byte[] magicNumber;
	private DongleType dongleType;
	private int gainCount;
	
	/**
	 * Creates a new instance
	 * 
	 * @param magicNumber
	 * @param tunerType
	 * @param gainCount
	 */
	public RtlTcpDongleInfo(byte[] magicNumber, int tunerType, int gainCount) {
		this.magicNumber = magicNumber;
		this.gainCount = gainCount;
		initDongleType(tunerType);
	}
	
	private void initDongleType(int tunerType) {
		switch (tunerType) {
		case 1:
			dongleType = DongleType.E4000;
			break;
		case 2:
			dongleType = DongleType.FC0012;
			break;
		case 3:
			dongleType = DongleType.FC0013;
			break;
		case 4:
			dongleType = DongleType.FC2580;
			break;
		case 5:
			dongleType = DongleType.R820T;
			break;
		case 6:
			dongleType = DongleType.R828D;
			break;
		default:
			dongleType = DongleType.UNKNOWN;
		}
	}
	
	public String getMagicNumber() {
		return new String(magicNumber);
	}
	
	public DongleType getDongleType() {
		return dongleType;
	}
	
	/**
	 * Gain Count useful for setting gain by index
	 * 
	 * @return
	 */
	public int getGainCount() {
		return gainCount;
	}
	
	/**
	 * Returns a string representation of the dongle information of the attached RTL-SDR,
	 * consists of the magic number, tuner type and gain count.
	 */
	@Override
	public String toString() {
		return "{MagicNumber: " +getMagicNumber() + " Tuner: " +dongleType + " GainCount: " +gainCount +"}";
	}
	
	/**
	 * Checks if the received magic number is 'valid'
	 * 
	 * @return
	 */
	public boolean isValid() {
		return VALID_MAGIC_NUMBER.equalsIgnoreCase(getMagicNumber());
	}

}
