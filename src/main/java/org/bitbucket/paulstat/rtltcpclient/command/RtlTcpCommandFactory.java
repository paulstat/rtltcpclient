/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient.command;

import org.bitbucket.paulstat.rtltcpclient.RtlTcpDongleInfo;

/**
 * Factory class for creating RtlTcpCommand instances
 *
 * @author Paul
 */
public final class RtlTcpCommandFactory {
	
	private RtlTcpDongleInfo dongleInfo;
	
	/**
	 * Creates a new instance
	 * 
	 * @param dongleInfo
	 */
	public RtlTcpCommandFactory(RtlTcpDongleInfo dongleInfo) {
		this.dongleInfo = dongleInfo;
	}
	
	/**
	 * Create a new command instance of the specified type with the specified value
	 * 
	 * @param commandType
	 * @param commandParam
	 * @return
	 * @throws RtlTcpCommandException 
	 */
	public RtlTcpCommand createCommand(RtlTcpCommandType commandType, int commandParam) throws RtlTcpCommandException {
		
		switch (commandType) {
		case SET_FREQUENCY:
			return new RtlTcpSetCenterFrequencyCommand(commandParam);
		case SET_FREQ_CORRECTION:
			return new RtlTcpSetFrequencyCorrectionCommand(commandParam);
		case SET_RTL_XTAL_FREQ:
			return new RtlTcpSetRtlXtalFrequencyCommand(commandParam);
		case SET_SAMPLE_RATE:
			return new RtlTcpSetSampleRateCommand(commandParam);
		case SET_TUNER_GAIN:
			return new RtlTcpSetTunerGainCommand(commandParam);
		case SET_TUNER_GAIN_BY_INDEX:
			return new RtlTcpSetTunerGainByIndexCommand(dongleInfo, commandParam);
		case SET_TUNER_IF_GAIN:
			return new RtlTcpSetTunerIFGainCommand(commandParam);
		case SET_TUNER_XTAL_FREQ:
			return new RtlTcpSetTunerXtalFrequencyCommand(commandParam);
		default:
			return new RtlTcpNullCommand(0);
		}
	}
	
	/**
	 * Create a new command instance of the specified type with the specified value
	 * 
	 * @param commandType
	 * @param commandParam
	 * @return
	 */
	public RtlTcpCommand createCommand(RtlTcpCommandType commandType, boolean commandParam) {
		
		switch (commandType) {
		case SET_AGC_MODE:
			return new RtlTcpSetAGCModeCommand(commandParam);
		case SET_DIRECT_SAMPLING:
			return new RtlTcpSetDirectSamplingCommand(commandParam);
		case SET_GAIN_MODE:
			return new RtlTcpSetGainModeCommand(commandParam);
		case SET_OFFSET_TUNING:
			return new RtlTcpSetOffsetTuningCommand(commandParam);
		case SET_TEST_MODE:
			return new RtlTcpSetTestModeCommand(commandParam);
		default:
			return new RtlTcpNullCommand(0);
		}
		
	}

}
