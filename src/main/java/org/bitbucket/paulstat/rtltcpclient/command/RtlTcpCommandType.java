/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient.command;

/**
 * RtlTcpCommandType
 *
 * @author Paul
 */
public enum RtlTcpCommandType {
	
	SET_FREQUENCY((byte)0x01),
	SET_SAMPLE_RATE((byte)0x02),
	SET_GAIN_MODE((byte)0x03),
	SET_TUNER_GAIN((byte)0x04),
	SET_FREQ_CORRECTION((byte)0x05),
	SET_TUNER_IF_GAIN((byte)0x06),
	SET_TEST_MODE((byte)0x07),
	SET_AGC_MODE((byte)0x08),
	SET_DIRECT_SAMPLING((byte)0x09),
	SET_OFFSET_TUNING((byte)0x0a),
	SET_RTL_XTAL_FREQ((byte)0x0b),
	SET_TUNER_XTAL_FREQ((byte)0x0c),
	SET_TUNER_GAIN_BY_INDEX((byte)0x0d),
	UNKNOWN_COMMAND;
	
	private byte cmd;
	
	private RtlTcpCommandType(byte cmd) {
		this.cmd = cmd;
	}
	
	private RtlTcpCommandType() {
		
	}
	
	/**
	 * Byte value of RtlTcpCommandType
	 * 
	 * @return
	 */
	public byte getCommandTypeValue() {
		return cmd;
	}

}
