/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient.command;

import org.bitbucket.paulstat.rtltcpclient.RtlTcpException;

/**
 * Signals that an exception of some kind has occurred with an RtlTcpCommand.
 *
 * @author Paul
 */
@SuppressWarnings("serial")
public class RtlTcpCommandException extends RtlTcpException {
	
	public RtlTcpCommandException() {
		
	}
	
	public RtlTcpCommandException(String message) {
		super(message);
	}
	
	public RtlTcpCommandException(String message, Throwable t) {
		super(message, t);
	}

}
