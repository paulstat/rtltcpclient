/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient.command;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Command to execute on the remote RtlTCP server
 *
 * @author Paul
 */
public abstract class RtlTcpCommand {
	
	private final RtlTcpCommandType commandType;
	
	private final int commandParam;
	
	/**
	 * Creates a new instance
	 * 
	 * @param os
	 * @param commandType
	 * @param commandParam
	 */
	public RtlTcpCommand(RtlTcpCommandType commandType, int commandParam) {
		this.commandType = commandType;
		this.commandParam = commandParam;
	}

	/**
	 * Command Type
	 * 
	 * @return
	 */
	public RtlTcpCommandType getCommandType() {
		return commandType;
	}

	/**
	 * Command Param value
	 * 
	 * @return
	 */
	public int getCommandParam() {
		return commandParam;
	}
		
	/**
	 * Convert RtlTcpCommand to String representation, consists of the commandType and commandValue
	 */
	@Override
	public String toString() {
		return commandType +": " + commandParam;
	}
	
	/**
	 * Execute the command on the specified Socket
	 * 
	 * @param socket
	 * @throws IOException 
	 */
	public void execute(Socket socket) throws IOException {
		OutputStream os = socket.getOutputStream();
		DataOutputStream dos = new DataOutputStream(os);

		ByteBuffer byteBuffer = ByteBuffer.allocate(5);
		byteBuffer.put(commandType.getCommandTypeValue());
		byteBuffer.putInt(commandParam);
		
		dos.write(byteBuffer.array());
	}

}
