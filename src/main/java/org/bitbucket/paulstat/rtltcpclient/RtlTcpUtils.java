/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommand;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandException;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandFactory;
import org.bitbucket.paulstat.rtltcpclient.command.RtlTcpCommandType;

/**
 * Utility class for interacting with Rtl Tcp.
 *
 * @author Paul
 */
public final class RtlTcpUtils {
	
	/**
	 * Can't be instantiated
	 */
	private RtlTcpUtils() {
		
	}
	
	/**
	 * Converts a RtlTcpConnectionConfig to a list of RtlTcpCommands
	 * 
	 * @param config to convert
	 * @param dongleInfo used by some RtlTcpCommands
	 * @return List<RtlTcpCommand>
	 * @throws RtlTcpCommandException 
	 */
	public static final List<RtlTcpCommand> toCommandList(RtlTcpConnectionConfig config, RtlTcpDongleInfo dongleInfo) throws RtlTcpCommandException {
		List<RtlTcpCommand> commandList = new ArrayList<>();
		
		RtlTcpCommandFactory commandFactory = new RtlTcpCommandFactory(dongleInfo);
		
		if (config.getSampleRate() > 0) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_SAMPLE_RATE, config.getSampleRate()));
		}
		
		if (config.getFrequencyCorrection() > 0 ) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_FREQ_CORRECTION, config.getFrequencyCorrection()));	
		}

		if (config.getCenterFrequency() > 0) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_FREQUENCY, config.getCenterFrequency()));
		}
		
		if (config.isAgcMode()) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_AGC_MODE, true));
		} else  {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_AGC_MODE, false));
		}
		
		if (config.isTunerGainMode()) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_GAIN_MODE, true));
		}
		
		if (config.getGainByIndex() > 0) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_TUNER_GAIN_BY_INDEX, config.getGainByIndex()));	
		}
		
		if (config.isDirectSampling()) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_DIRECT_SAMPLING, true));
		}
				
		if (config.isOffsetTuning()) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_OFFSET_TUNING, true));
		}
		
		if (config.getRtlXtalFrequency() > 0) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_RTL_XTAL_FREQ, config.getRtlXtalFrequency()));
		}
		
		if (config.isTestMode()) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_TEST_MODE, true));
		}
		
		if (config.getTunerGain() > 0) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_TUNER_GAIN, config.getTunerGain()));
		}
		
		if (config.getTunerXtalFrequency() > 0) {
			commandList.add(commandFactory.createCommand(RtlTcpCommandType.SET_TUNER_XTAL_FREQ, config.getTunerXtalFrequency()));
		}

		return commandList;
	}

}
