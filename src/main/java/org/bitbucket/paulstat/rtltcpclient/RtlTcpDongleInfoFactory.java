/*
  Copyright 2017 Paul Statham

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package org.bitbucket.paulstat.rtltcpclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Reads header information from a Socket connection to an RTL-SDR dongle
 * and creates a new RtlTcpDongleInfo instance from it.
 *
 * @author Paul
 */
public final class RtlTcpDongleInfoFactory {
	
	private static final int MAGIC_NUMBER_LENGTH = 4;
	private static final int RTL_TCP_HEADER_LENGTH = 12;
	private static final int TUNER_TYPE_INDEX = 7;
	private static final int GAIN_COUNT_INDEX = 11;
	
	/**
	 * Can't be instantiated
	 */
	private RtlTcpDongleInfoFactory() {
		
	}
	
	/**
	 * Reads the RtlTcpDongleInfo from the socket
	 * 
	 * @param socket
	 * @return
	 * @throws IOException
	 */
	public static RtlTcpDongleInfo createRtlTcpDongleInfo(Socket socket) throws IOException {
		
		byte[] magicNumber = new byte[MAGIC_NUMBER_LENGTH];
		int tunerType = 0;
		int gainCount = 0;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		for (int i=0; i < RTL_TCP_HEADER_LENGTH; i++) {
			int next = reader.read();
			if (socketClosed(next)) {
				throw new IOException("Closed connection detected");
			}
			if (i < MAGIC_NUMBER_LENGTH) {
				magicNumber[i] = (byte) next;
			} else {
				if (i == TUNER_TYPE_INDEX) {
					tunerType = next;
				} else if (i == GAIN_COUNT_INDEX) {
					gainCount = next;
				}
			}
		}
		
		return new RtlTcpDongleInfo(magicNumber, tunerType, gainCount);
	}
	
	/**
	 * Checks the status of the socket connection
	 * if the read is -1 then there is no more input and the 
	 * connection is lost.
	 * 
	 * @param next
	 * @return
	 */
	private static boolean socketClosed(int next) {
		return next == -1;
	}

}
